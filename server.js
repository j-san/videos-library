require('dotenv').load();

var mongoose = require('mongoose'),
    app = require('./src/app');

var port = process.env.PORT || 1337,
    mongodbUrl = process.env.MONGOLAB_URI || 'mongodb://localhost/video-library';

if (app.env === 'production' || app.env === 'staging') {
    require('newrelic');
}

mongoose.connect(mongodbUrl, function (err) {
    if (err) {
        throw err;
    }
    app.listen(port);
    console.log("Server running on port " + port);
});

