var stream = require('stream'),
    r = require('koa-route'),
    body = require('koa-body'),
    Video = require('./models/video');


exports.route = function (app) {
    app.use(body({
        multipart: true,
    }));

    app.use(function* (next) {
        this.type = 'json';
        yield next;
    });

    app.use(r.get('/api/videos', function* () {
        this.body = yield Video.find({}).exec();
    }));

    app.use(r.get('/api/video/:id', function* (id) {
        this.body = yield Video.findById(id).exec();
        if (!this.body) {
            this.status = 404;
            this.body = {};
        }
    }));

    app.use(r.patch('/api/video/:id', function* (id) {
        var video = yield Video.findById(id).exec();
        video.set(this.request.body);
        this.body = yield video.save();
    }));

    app.use(r.delete('/api/video/:id', function* (id) {
        yield Video.findById(id).findOneAndRemove().exec();
        this.body = {};
    }));

    app.use(r.get('/api/video/:id/status', function* (id) {
        var video = yield Video.findById(id).exec();

        try {
            yield video.s3status();
        } catch (e) {
            console.log('AWS Error...', e.message);
            throw e;
        }
        this.body = video;
    }));

    app.use(r.post('/api/upload', function* () {
        var file = this.request.body.files.file;

        var video = new Video({
            name: file.name,
            title: file.name,
            type: file.type
        });
        video = yield video.save();

        try {
            yield video.s3upload(file);
            yield video.s3transform();
        } catch (e) {
            console.log('AWS Error...', e.message);
            yield video.remove();
            throw e;
        }
        this.body = video;
    }));
};
