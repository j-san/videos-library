define(function () {
    return {
        init: function (parent, callback) {
            var fileInput = document.createElement("input");
            fileInput.setAttribute("type", "file");
            fileInput.setAttribute("multiple", "multiple");

            fileInput.style.visibility = "hidden";
            fileInput.style.position = "absolute";
            fileInput.style.top = "0";
            fileInput.style.left = "0";
            fileInput.style.right = "0";
            fileInput.style.bottom = "0";

            parent.style.position = "relative";
            parent.appendChild(fileInput);
            fileInput.addEventListener("change", function () {
                var files = fileInput.files;
                for (var i in files) {
                    var file = files[i];
                    callback(file);
                }
            });
        }
    };
});