require.config({
  paths: {
    views: "../views",
    knockout: "../components/knockout/dist/knockout",
    requirejs: "../components/requirejs/require",
    jquery: "../components/jquery/dist/jquery",
    bootstrap: "../components/bootstrap/dist/js/bootstrap",
    text: "../components/requirejs-text/text",
    "requirejs-text": "../components/requirejs-text/text",
    dropzone: "../components/dropzone/downloads/dropzone-amd-module"
  },
  packages: [

  ],
  shim: {
    "bootstrap": ["jquery"]
  }
});

require(['main'], function(){});
