define(['knockout', 'jquery', 'dropzone', 'bootstrap'], function (ko, $, Dropzone) {

    Dropzone.autoDiscover = false;
    var dz = new Dropzone('.dropzone', {
        previewsContainer: false,
        acceptedFiles: 'video/avi,video/mpeg,video/mp4,video/ogg,video/quicktime,video/webm'
    });

    dz.on('sending', function (file) {
        var video = {
            status: 'uploading',
            file: file
        };
        app.videos.push(video);
    });
    dz.on('success', function (file, result) {
        var video = ko.utils.arrayFirst(app.videos(), function (item) {
            return file === item.file;
        });
        video.model.set(result);
        video.model.checkStatus();
    });
    dz.on('error', function (file, errorMessage) {
        window.alert(errorMessage);
    });

    function AppModel() {
        this.videos = ko.observableArray([]);
        this.loadVideos();
        this.playerEnabled = ko.observable(true);
        this.currentVideo = ko.observable('');
    }


    AppModel.prototype.loadVideos = function () {
        var self = this;
        $.get('/api/videos').then(function (results) {
            self.videos(results);
        });
    };

    AppModel.prototype.openVideoPlayer = function (video) {
        this.currentVideo(video);
        $('#player').modal();
    };

    var app = new AppModel();


    return app;
});
