[ ![Codeship Status for j-san/videos-library](https://codeship.com/projects/70acea90-647a-0132-af74-0639b0c195d6/status?branch=master)](https://codeship.com/projects/52597)

Simple app to upload videos, compress them to low quality and then play them.


Back End:

* Koa
* Mongoose
* MongoDB
* Mocha / Chai / Supertest
* Gulp
* AWS S3 for storage
* AWS Elastic Transcoder for video format encoding


Front End:

* Knockout
* RequireJS
* Bootstrap
* jQuery
* Less
* Dropzone
* Bower

Install
-------


```
npm i
```

Dev
---

```
gulp watch-less
```

Dist
----

```
gulp dist
```
